using System.Collections.Generic;
using UnityEngine;

namespace ISN
{
    public class FoodSpawner : MonoBehaviour
    {
        [SerializeField] private Food[] _foodPrefabs;
        [SerializeField] private int _spawnCount;
        [SerializeField] private float _spawnDistance;

        public List<Food> SpawnedFood { get; private set; } = new List<Food>();

        private void Start() => SpawnFood();

        private void SpawnFood()
        {
            for (int i = 0; i < _spawnCount; i++)
            {
                var food = Instantiate(_foodPrefabs[Random.Range(0, _foodPrefabs.Length)], GetRandomPosition(), Quaternion.identity, transform);
                food.OnFoodStacked += RemoveFoodFromList;
                SpawnedFood.Add(food);
            }
        }

        private void RemoveFoodFromList(Food food)
        {
            food.OnFoodStacked -= RemoveFoodFromList;
            SpawnedFood.Remove(food);

            if (SpawnedFood.Count < 3)
                SpawnFood();
        }

        private Vector3 GetRandomPosition()
        {
            Vector2 insideUnitCircle = Random.insideUnitCircle; 
            Vector3 direction = new Vector3(insideUnitCircle.x, 0, insideUnitCircle.y);
            return transform.position + direction * _spawnDistance;
        }
    }

}
