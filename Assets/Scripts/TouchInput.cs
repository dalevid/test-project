using UnityEngine;
using UnityEngine.UI;

namespace ISN
{
    public class TouchInput : MonoBehaviour
    {
        [SerializeField] private float _maxTouchShift;
        [SerializeField] private Image _touchPadStick;
        [SerializeField] private Image _touchPadCircle;

        private Vector2 _firstTouch;
        private Vector2 _inputVector;
        private Vector2 _deltaInputVector;
        private float _currentTouchShift;

        public Vector3 GetDeltaInputVector()
        {
            if (Input.touchCount > 0)
            {
                _deltaInputVector = ReadInputDeltaVector(Input.GetTouch(0)).normalized;
                return new Vector3(_deltaInputVector.x, 0, _deltaInputVector.y);
            }
            else
                return Vector3.zero;
        }

        private Vector2 ReadInputDeltaVector(Touch touch)
        {
            switch (touch.phase)
            {
                case TouchPhase.Began:
                    _firstTouch = touch.position;
                    ShowTouchPad(_firstTouch);
                    return Vector2.zero;

                case TouchPhase.Moved:
                case TouchPhase.Stationary:
                    _currentTouchShift = Vector2.Distance(touch.position, _firstTouch);
                    _inputVector = (_currentTouchShift < _maxTouchShift) ? touch.position : Vector2.Lerp(_firstTouch, touch.position, _maxTouchShift / _currentTouchShift);
                    MoveTouchPad(_inputVector);
                    return _inputVector - _firstTouch;

                case TouchPhase.Ended:
                    HideTouchPad();
                    return Vector2.zero;

                default:
                    return Vector2.zero;
            }
        }

        private void ShowTouchPad(Vector2 touchPosition)
        {
            _touchPadCircle.transform.position = touchPosition;
            _touchPadStick.transform.position = touchPosition;
            _touchPadStick.gameObject.SetActive(true);
            _touchPadCircle.gameObject.SetActive(true);
        }

        private void MoveTouchPad(Vector2 touchPosition)
        {
            _touchPadStick.transform.position = touchPosition;
        }

        private void HideTouchPad()
        {
            _touchPadStick.gameObject.SetActive(false);
            _touchPadCircle.gameObject.SetActive(false);
        }
    }
}

