using UnityEngine;

namespace ISN
{
    public class Eater : MonoBehaviour
    {
        public event System.Action<float, float> OnColoriesAdded;
        public event System.Action OnColoriesMaxCountReached;
        public event System.Action<Eater> OnEaterFull;

        [SerializeField] private float _caloriesMaxCount;
        [SerializeField] private Animator _animator;
        [SerializeField] private ParticleSystem _crumbs;

        private bool _crumbsIsPlaying = false;
        private float _currentCaloriesCount = 0;
        private Vector3 _fatScale;

        [field: SerializeField] public Transform FoodDestination { get; private set; }

        private void Start()
        {
            _fatScale = transform.localScale;
        }

        public void AddCalories(float calories)
        {
            if (calories < 0)
                throw new System.Exception("Colories need to be above zero");
 
            _currentCaloriesCount += calories;

            if (_currentCaloriesCount < _caloriesMaxCount)
            {
                AddFat();
                PlayCrumbs();
                OnColoriesAdded?.Invoke(_currentCaloriesCount, _caloriesMaxCount);
            }
            else
            {
                StopCrumbs();
                _currentCaloriesCount = _caloriesMaxCount;
                OnColoriesMaxCountReached?.Invoke();
                OnEaterFull?.Invoke(this);
                _animator.Play("Waving");
            }
        }

        private void AddFat()
        {
            _fatScale.x += 0.01f;
            transform.localScale = _fatScale;
        }

        private void PlayCrumbs()
        {
            if (_crumbsIsPlaying)
                return;

            _crumbsIsPlaying = true;
            _crumbs.Play();
        }

        public void StopCrumbs()
        {
            _crumbsIsPlaying = false;
            _crumbs.Stop();
        }
    }
}

