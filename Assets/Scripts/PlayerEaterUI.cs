using UnityEngine;
using UnityEngine.UI;

namespace ISN
{
    public class PlayerEaterUI : MonoBehaviour
    {
        [SerializeField] Eater _eater;
        [SerializeField] Image _image;

        private void OnEnable()
        {
            _image.fillAmount = 0;
            _eater.OnColoriesAdded += RefreshUI;
        }

        private void OnDisable()
        {
            _eater.OnColoriesAdded -= RefreshUI;
        }

        private void RefreshUI(float currentCalories, float maxCalories)
        {
            _image.fillAmount = currentCalories / maxCalories;
        }
    }
}

