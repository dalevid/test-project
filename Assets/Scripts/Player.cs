using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

namespace ISN
{
    public class Player : MonoBehaviour
    {

        [SerializeField] private Transform _stackStartPoint;
        [SerializeField] private float _foodStackSpeed;
        [SerializeField] protected int _stackMaxCount;
        [SerializeField] protected Animator _animator;

        private Vector3 _foodPositionShift;
        private bool _eaterIsFull;
        private Sequence _feedSequence;
        protected List<Food> _foodList = new List<Food>();

        private void StopFeeding(Eater eater)
        {
            eater.OnEaterFull -= StopFeeding;
            _eaterIsFull = true;
            _feedSequence?.Kill();
            _animator.SetLayerWeight(1, 0);
            foreach (var food in _foodList)
                Destroy(food.gameObject);

            _foodList.Clear();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.TryGetComponent(out Food food) && _foodList.Count < _stackMaxCount && _eaterIsFull == false)
            {
                _animator.SetLayerWeight(1, 1);
                StartCoroutine(StackRoutine(food));
            }

            if (other.TryGetComponent(out Eater eater) && _foodList.Count != 0 && _eaterIsFull == false)
            {
                eater.OnEaterFull += StopFeeding;
                FeedEater(eater);
            }
        }

        private IEnumerator StackRoutine(Food food)
        {
            if (_foodList.Count >= _stackMaxCount)
                yield break;

            Vector3 foodTargetShift = FoodPositionShift(_foodList.Count);
            _foodList.Add(food);
            food.OnStackFood();
            food.transform.SetParent(_stackStartPoint, true);

            while (food != null && Vector3.Distance(food.transform.localPosition, Vector3.zero + foodTargetShift) > 0)
            {
                food.transform.localPosition = Vector3.MoveTowards(food.transform.localPosition, Vector3.zero + foodTargetShift, Time.deltaTime * _foodStackSpeed);
                yield return null;
            }
        }

        private Vector3 FoodPositionShift(int index)
        {
            if (index == 0)
                _foodPositionShift = Vector3.zero;
            else
                _foodPositionShift += new Vector3(0, _foodList[index - 1].GetComponent<MeshRenderer>().bounds.size.y, 0);

            return _foodPositionShift;
        }

        private void FeedEater(Eater eater)
        {
            _feedSequence?.Kill();
            _feedSequence = DOTween.Sequence();
            _foodList.Reverse();
            for (int i = 0; i < _foodList.Count; i++)
                FoodFlyStart(_foodList[i], eater);

            _feedSequence.OnComplete(() =>
            {
                foreach (var piece in _foodList)
                    Destroy(piece.gameObject);
                eater.StopCrumbs();
                _foodList.Clear();
                _animator.SetLayerWeight(1, 0);
            });
        }

        private void FoodFlyStart(Food food, Eater eater)
        {
            _feedSequence.Append(food.transform.DOMove(eater.FoodDestination.position, 0.1f).OnComplete(()=>
                {
                    eater.AddCalories(food.Calories);
                    _foodList.Remove(food);
                    Destroy(food.gameObject);
                }));
        }
    }
}

