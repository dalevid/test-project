using UnityEngine;

namespace ISN
{
    [RequireComponent(typeof(MeshRenderer))]
    [RequireComponent(typeof(Collider))]

    public class Food : MonoBehaviour
    {
        public event System.Action<Food> OnFoodStacked;

        private Collider _collider;
        [field: SerializeField] public float Calories { get; private set; }
        public bool IsTaken { get; private set; } = false;

        public void DisableFoodCollider()
        {

        }
        public void OnStackFood()
        {
            OnFoodStacked?.Invoke(this);
            _collider.enabled = false;
            IsTaken = true;
        }

        private void Awake()
        {
            _collider = GetComponent<Collider>();
        }
    }

}

