using UnityEngine;

namespace ISN
{
    public class PlayerMovement : MonoBehaviour
    {
        [SerializeField] private TouchInput _input;
        [SerializeField] private float _speed;
        [SerializeField] private Animator _animator;

        private Vector3 _movementDirection;

        private void Update()
        {
            Move();
            LookAtDirection();
        }

        private void Move()
        {
            _movementDirection = _input.GetDeltaInputVector();
            if (_movementDirection != Vector3.zero)
            {
                transform.position += _speed * Time.deltaTime * _movementDirection;
                _animator.Play("Run");
            }
            else
            {
                _animator.Play("Idle");
            }

        }

        private void LookAtDirection()
        {
            if (_movementDirection != Vector3.zero)
                transform.rotation = Quaternion.LookRotation(_movementDirection);
        }
    }
}

