using System.Collections;
using UnityEngine;
using UnityEngine.AI;

namespace ISN
{
    public class EnemyAI : Player
    {
        [SerializeField] private NavMeshAgent _agent;
        [SerializeField] private FoodSpawner _spawner;
        [SerializeField] private Transform _feedTarget;
         
        private bool _destinationNotReached;
        private Food _targetFood;
        private Coroutine _travelRoutine;

        private void Start()
        {
            _travelRoutine = StartCoroutine(TravelRoutine());
        }

        private IEnumerator TravelRoutine()
        {
            SetTargetFood();
            _animator.Play("Run");
            _destinationNotReached = true;

            while (true)
            {
                if (_agent.remainingDistance < 0.05f)
                {
                    _animator.Play("Idle");
                    yield return new WaitForSeconds(0.2f);
                    
                    _animator.Play("Run");

                    if (_foodList.Count < _stackMaxCount)
                        SetTargetFood();
                    else
                        _agent.destination = _feedTarget.position;
                }

                if (_targetFood.IsTaken)
                {
                    SetTargetFood();
                }
                yield return null;
            }
        }
        private void SetTargetFood()
        {
            _targetFood = _spawner.SpawnedFood[Random.Range(0, _spawner.SpawnedFood.Count)];
            _agent.destination = _targetFood.transform.position;
        }

        //private void OnTriggerEnter(Collider other)
        //{
        //    if (other.TryGetComponent(out Food food))
        //        TakeFoodUntilLimit();

        //    if (other.TryGetComponent(out Eater eater))
        //        StartCoroutine(WaitFeedRoutine());
        //}



        //private void TakeFoodUntilLimit()
        //{

        //    _animator.Play("Run");
        //    _destinationNotReached = true;
        //}

        //private IEnumerator WaitFeedRoutine()
        //{
        //    yield return new WaitForSeconds(0.7f);
        //    _animator.Play("Run");
        //}
    }
}

